﻿using System;
using System.Collections.Generic;

namespace DataTables.AspNetCore.Models
{
    public class SampleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public SampleModel() { }
        public SampleModel(int id, string name)
        {
            Id = id;
            Name = name;
        }


        private static IEnumerable<SampleModel> SampleData;
        public static IEnumerable<SampleModel> GetSampleData()
        {
            if (SampleModel.SampleData == null)
            {
                var _data = new List<SampleModel>(53);

                var random = new Random();
                for (int i = 0; i < 53; i++)
                {
                    var letter1 = random.Next(65, 91);
                    var letter2 = random.Next(65, 91);
                    var letter3 = random.Next(65, 91);
                    var letter4 = random.Next(65, 91);

                    var sampleEntity = new SampleModel(i + 1, new string(new[] { (char)letter1, (char)letter2, (char)letter3, (char)letter4 }));
                    _data.Add(sampleEntity);
                }

                SampleModel.SampleData = _data;
            }

            return SampleModel.SampleData;
        }
    }
}
